<?php

Route::group([
	'middleware' => ['web','auth'],
	'prefix' => 'backend', 
	'namespace' => 'Modules\Core\Http\Controllers'],
	function()
	{
	    Route::get('/dashboard', 'CoreController@index');
	});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

