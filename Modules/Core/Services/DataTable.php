<?php  

namespace Modules\Core\Services;


use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Yajra\Datatables\Contracts\DataTableButtonsContract;
use Yajra\Datatables\Contracts\DataTableContract;
use Yajra\Datatables\Contracts\DataTableScopeContract;
use Yajra\Datatables\Services\DataTable as DatatableBase;
use Yajra\Datatables\Transformers\DataTransformer;


class DataTable extends DataTableBase{

     protected $printPreview = 'datatables::print';
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
    
    }

     protected $actions = ['csv', 'excel', 'pdf', 'print'];

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom' => 'Bfrtip',
                        'buttons'=>['pageLength','csv', 'excel', 'pdf', 'print', 'reload'],
                        'lengthMenu' => [
                            [ 10, 25, 50, -1 ],
                            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                        ]         
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {

    }

    protected function generateAction($id)
    {
        return str_replace('_ID_',$id,collect($this->action)->map(function($attr,$action) use($id){
            extract(array_merge(
                    ['route'=>'','permission'=>'','javascript'=>''],
                    $attr));
             if(\Auth::user()->can($permission) || \Auth::user()->is_super_admin){
                return $this->{$action."Action"}(route($route, $id), $javascript);
             }
        })->implode(''));
    }

    protected function deleteAction($route, $javascript = ""){
        
        $javascript = $javascript ?: "onclick=\"javascript:clickDestroy(this); return false;\"";
        
        return "<a  {$javascript} data-item=\"__ID__\" href=\"{$route}\" 
                class=\"btn-action btn btn-icon btn-danger btn-xs\" title='".lang('Delete')."'>
                    <i class=\"fa fa-trash-o\"></i>
                </a>";
    }

    protected function showAction($route ,$javascript = ""){

        return "<a  {$javascript} data-item=\"__ID__\" href=\"{$route}\" 
            class=\"btn-action btn btn-icon btn-info btn-xs\" title='".lang('Detail')."'>
                <i class=\"fa fa-search\"></i>
            </a>";
    }

    protected function editAction($route, $javascript = ""){
        
        return "<a  {$javascript} data-item=\"__ID__\" href=\"{$route}\" 
            class=\"btn-action btn btn-icon btn-primary btn-xs\" title='".lang('Edit')."'>
                <i class=\"fa fa-edit\"></i>
            </a>";
    }

    protected function status($id,$status)
    {
        $html = "";
        if($status==1)
        {
            $html.= '<button type="button" class="btn btn-success btn-xs">'.lang('Enable').'</button>';
        }
        else 
        {
            $html.= '<button type="button" class="btn btn-danger btn-xs">'.lang('Disable').'</button>';
        }
        return $html;
    }
}