<?php

namespace Modules\Core\Supports\Traits;

trait RepositorieableTrait
{

	public function query()
	{
		return $this->model->query();
	}
}
