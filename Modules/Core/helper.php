<?php 

if (!function_exists('layout')) {
    /**
     * Get path of the theme in dot notation
     * @example  @extends(layout('backend.layout'))
     * @param  string $path
     * @return string 
     */
    function layout($path){
        return app('themes')->getNamespace($path);
    }
}

if (!function_exists('lang')) {
    /** 
     * Get the translation.
     *
     * @param  string text
     *
     * @return string
     */
    function lang($text, $parametters = []) 
    {   
        $prefix = str_replace(
            'php','',
            config('core.translator.filename')
        );

        $translated = trans("{$prefix}".$text, $parametters);
        return \Illuminate\Support\Str::contains($translated, $prefix) ? $text : $translated;
    }   
}

if (!function_exists('resource_name')) 
{
    function resource_name($prefix, $custom_methods = []) 
    {
        $default_methods = [
            'index'   => $prefix . '.index',
            'create'  => $prefix . '.create',
            'store'   => $prefix . '.store',
            'show'    => $prefix . '.show',
            'edit'    => $prefix . '.edit',
            'update'  => $prefix . '.update',
            'destroy' => $prefix . '.destroy'
        ];

        return array_merge($default_methods, 
            array_map(function($method) use($prefix){
                return "{$prefix}.{$method}";
            },$custom_methods)
        );
    };
}