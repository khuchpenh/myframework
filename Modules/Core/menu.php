<?php
Menu::create('sidebar', function($menu) {
    $menu->setPresenter( Modules\Core\Presenters\Bootstrap\SidebarMenuPresenter::class);
    $menu->url('backend/dashboard', 'Dashboard', 0, ['icon' => 'fa fa-home']);
    $menu->dropdown('Settings', function ($sub) {
        $sub->url('settings/account', 'Account');
        $sub->url('settings/password', 'Password');
        $sub->url('settings/design', 'Design');
    }, 1, ['icon' => 'fa fa-home']);
});