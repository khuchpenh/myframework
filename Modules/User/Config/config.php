<?php

return [
    'name' => 'User',
    'permission' => ['view','create','edit','delete']
];
