<?php

namespace Modules\User\DataTables;

use Modules\Core\Services\DataTable;
use Modules\User\Entities\Role;
use Modules\User\Repositories\UserRepository;

class UserDataTable extends DataTable
{
     protected $printPreview = 'datatables::print';

     protected $action = [
        'edit' =>[
            'route' => 'user.edit',
            'permission'=>'user.edit'
        ],
        'delete' =>[
            'route' => 'user.destroy',
            'permission'=>'user.delete'
        ],
     ];
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('fullname',function($row){
                $fullname = $row->profile()->first();
                return $row?$row->name : "";
            })
            ->editColumn('role',function($row){
                $role = $row->roles()->first();
               return $role?$role->name:'';
            })
            ->editColumn("is_active",function($row){
                return $this->status($row->id,$row->is_active);
            })
            ->addColumn('action', function($row){
                return $this->generateAction($row->id);
            })
            ->rawColumns(['is_active','action']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        return $this->applyScopes(
                app(UserRepository::class)->listUser()
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
                [
                    'title'=>lang('id'),
                    'name'=>'id',
                    'data'=>'id'
                ],
                [
                    'title'=>lang('Full Name'),
                    'name'=>'fullname',
                    'data'=>'fullname',
                    'searchable'=>false
                ],
                [
                    'title'=>lang('Email'),
                    'name'=>'email',
                    'data'=>'email'
                ],
                [
                    'title'=>lang('Role'),
                    'name'=>'role',
                    'data'=>'role',
                    'searchable'=>false
                ],
                [
                    'title'=>lang('Status'),
                    'name'=>'is_active',
                    'data'=>'is_active'
                ],
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'role_' . time();
    }
}
