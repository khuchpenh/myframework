<?php

namespace Modules\User\DataTables;

use Modules\Core\Services\DataTable;
use Modules\User\Entities\Role;
// use Yajra\Datatables\Services\DataTable;

class roleDataTable extends DataTable
{
     protected $printPreview = 'datatables::print';

     protected $action = [
        'edit' =>[
            'route' => 'role.edit',
            'promission'=>'role:edit'
        ],
        'delete' =>[
            'route' => 'role.destroy',
            'promission'=>'role:delete'
        ],
     ];
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($row){
                return $this->generateAction($row->id);
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Role::query();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom' => 'Bfrtip',
                        'buttons'=>['pageLength','csv', 'excel', 'pdf', 'print', 'reload'],
                        'lengthMenu' => [
                            [ 10, 25, 50, -1 ],
                            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                        ]         
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
                [
                    'title'=>lang('id'),
                    'name'=>'id',
                    'data'=>'id'
                ],
                [
                    'title'=>lang('Role Name'),
                    'name'=>'name',
                    'data'=>'name'
                ]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'role_' . time();
    }
}
