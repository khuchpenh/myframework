<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class Profile extends Model
{
    protected $fillable = [
    	'first_name',
    	'last_name',
    	'dob',
    	'phone'
    ];

    public function profileable()
    {
    	 return $this->morphTo();
    }
}
