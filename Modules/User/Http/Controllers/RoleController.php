<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Session\flash;
use Kris\LaravelFormBuilder\FormBuilder;
use Modules\User\DataTables\UsersDataTable;
use Modules\User\DataTables\roleDataTable;
use Modules\User\Entities\Permission;
use Modules\User\Entities\Role;
use Modules\User\Http\Requests\RoleForm;
use Modules\User\Repositories\RoleRepository;
use Nwidart\Modules\Exeptions\ModuleNotFoundException;
use Nwidart\Modules\Facades\Module;

class RoleController extends Controller
{

    public function __construct(RoleRepository $role){
        $this->role = $role;
    }

    public function index(roleDataTable $dataTable)
    {
        $title = lang('Role');
        return $dataTable->render('user::role.index',compact('title'));
    }

    public function create(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(RoleForm::class,[
                'method'    =>  'POST',
                'url'       =>  route('role.store'),
                'role'      =>  'form',
                'class'     =>  'form-horizontal',
                'enctype'   =>  'multipart/form-data',
            ]);
        $title = lang('Create Role');
        return view('user::role.create',compact('form','title'));
    }

    public function store(Request $request,FormBuilder $formBuilder)
    {
        $data = array_only($request->all(),['name']);
        $roleName = $request->name;
        $permissions = $request->permission;
        $id = $request->id;
        $form = $formBuilder->create(RoleForm::class);
        if(!$form->isValid()){
            return redirct()->back()
                            ->withErrors($form->getErrors())
                            ->withInput($data);
        }
        if($id=='')
        {
            $role = Role::create($data);
        }
        else 
        {
          $role = Role::find($id)->update($data); 
        }
        if($id) {
            $role = Role::where('name',$roleName)->first();
        }
        // delete role and reinsert
        $role->permissions()->detach();
        if($permissions){
            foreach($permissions as $key => $value){
                $exsitpermission = Permission::where('name',$value)->first();

                if(!$exsitpermission){

                    Permission::create(['name'=>$value]);
                }

                $role->givePermissionTo($value);
            }
        }
        flash()->success("Your data was save");
        return redirect(route('role.index'));
        
    }


    public function edit($id,FormBuilder $formBuilder)
    {
        $model = $this->role->find($id);
        $form = $formBuilder->create(RoleForm::class,[
                'method'    =>  'POST',
                'url'       =>  route('role.store'),
                'role'      =>  'form',
                'class'     =>  'form-horizontal',
                'enctype'   =>  'multipart/form-data',
                'novalidate'=>  'novalidate',
                'model'=>$model
            ]);
        $title = lang('Edit Role');
        $html = $this->listPermission($id);
        return view('user::role.create',compact('form','title','html'));
    }

    public function destroy()
    {
    }

    public function listPermission($role)
    {
        $role = $this->role->find($role);
        $rolePermmission = $role->permissions()->get();
        $modules = Module::enabled();
        if($modules)
        {
            foreach($modules as $key => $module){
                $moduleName = $module->getLowerName();
                $allPermission[$moduleName]=\Config::get($moduleName.'.permission');
            }
        }
        $html ="";
        
        if($allPermission)
        {
            
            foreach($allPermission as $key => $value)
            {
                $html = $html ."<div class=\"panel panel-default\">
                                  <div class=\"panel-heading\">
                                    <h4 class=\"panel-title\">
                                      <a data-toggle=\"collapse\" href=\"#".$key."\">".$key."</a>
                                    </h4>
                                  </div>
                                  <div id=\"".$key."\" class=\"panel-collapse collapse\">
                                    <div class=\"panel-body\">" ;
                $checkbox = '';
                if($value){
                    $html .= view('user::role.permission',compact('value','key','rolePermmission'))->render();  
                }
                $html = $html . "</div>
                        </div>
                    </div>
                </div>";
            }

            
        }

        return $html;
    }
}
