<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use Modules\User\DataTables\UserDataTable;
use Modules\User\Entities\Role;
use Modules\User\Repositories\UserRepository;

class UserController extends Controller
{

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(UserDataTable $dataTable)
    {
        $model = 'user';
        $title = lang('List User');
        return $dataTable->render('user::index',compact('title','model'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(FormBuilder $formBuilder)
    {
        
        $form = $formBuilder->create("Modules\User\Http\Requests\UserForm",[
                'method'    =>  'POST',
                'url'       =>  route('user.store'),
                'role'      =>  'form',
                'class'     =>  'form-horizontal',
                'enctype'   =>  'multipart/form-data',
            ]);
        $model = 'user';
        $title = lang('Create User');
        return view('user::create',compact('form','title','model'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request,FormBuilder $formBuilder)
    {
        $form = $formBuilder->create("Modules\User\Http\Requests\UserForm");
         $data = [
            'user_name'=>$request->user_name,
            'is_super_admin' => true,
            'is_active'=>true,
            'name'=>'penh',
            'email' => $request->email,
            'password' => bcrypt('111111'),
            ];
        $profile = [
            'first_name'=>$request->first_name,
            'last_name' => $request->last_name,
            //'dob' => $request->dob,
            'phone' => $request->phone
            ];
        $id = $request->id;
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        if($id=="")
        {
            $user = $this->user->create($data);
            $user->assignRole(Role::findByName($request->role, 'web'));
            $user->profile()->create($profile);
        }
        else 
        {
            $user = $this->user->update($data,$id);
            $user = $this->user->find($id);
            $user->profile()->update($profile);
        }
        flash()->success("Your data was save");
        return redirect(route('user.index'));
    }   

    public function edit(FormBuilder $formBuilder,$id)
    {
        $model = $this->user->find($id);
        $form = $formBuilder->create("Modules\User\Http\Requests\UserForm",[
                'method'    =>  'POST',
                'url'       =>  route('user.store'),
                'role'      =>  'form',
                'class'     =>  'form-horizontal',
                'enctype'   =>  'multipart/form-data',
                'model'     => $model
            ]);
        $title = lang('Edit').' '.lang('User');
        $model = 'user';
        return view('user::create',compact('form','title','model'));
    }

    public function destroy(Request $request)
    {
        $check = $this->user->find($request->id);
        $dataExist = 0;
        $success = 0;
        if(\Auth::user()->id==$check->id)
        {
          return ++$dataExist;
        }
        else 
        {
            $this->user->delete($request->id);
            $success++;
        }
       return  $success;
    }
}
