<?php

namespace Modules\User\Http\Requests;

use Kris\LaravelFormBuilder\Form;

class RoleForm extends Form
{
    public function buildForm()
    {
        $this
        	->add('id','hidden')
        	->add('name','text',[
        			'label' => lang('Name'),
        			'rules' =>"required"
        		]);
    }
}
