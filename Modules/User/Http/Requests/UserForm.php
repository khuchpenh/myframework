<?php

namespace Modules\User\Http\Requests;

use Kris\LaravelFormBuilder\Form;
use Modules\User\Entities\Role;

class UserForm extends Form
{
    public function buildForm()
    {
        $roles = Role::pluck('name','name')->toArray();
        // dd($roles);
        $this
        	->add('id','hidden')
        	->add('user_name','text',[
        			'label' => lang('User Name'),
        			'rules' =>"required",
        		])
        	->add('email','text',[
        			'label'=>lang('Email'),
        			'rules'=>'required|email'
        		])
        	->add('role','select',[
        			'label'=>lang('Role'),
                    'choices'=> $roles,
        			'rules'=>'required'
        		])
        	->add('first_name','text',[
        			'label'=>lang('First Name'),
        			'rules'=>'required'
        		])
        	->add('last_name','text',[
        			'label'=>lang('Last Name'),
        			'rules'=>'required'
        		])
        	->add('dob','text',[
        			'label'=>lang('Date of Birth'),
                    'attr' => ['id'=>'dob_at']
        		])
        	->add('phone','text',[
        			'label'=>lang('Phone')
        		]);
    }
}
