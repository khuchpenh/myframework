<?php

Route::group([
	'middleware' => ['web'], 
	'prefix' => 'backend', 
	'namespace' => 'Modules\User\Http\Controllers'], 
	function()
	{
   		Route::resource('user', 'UserController',['names' => resource_name('user')]);

   		Route::resource('role','RoleController',['name'=>resource_name('role')]);
	});
// Route::get('/', 'HomeController@index');
