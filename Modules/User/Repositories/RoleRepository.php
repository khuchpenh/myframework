<?php 

namespace Modules\User\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Modules\Core\Supports\Traits\RepositorieableTrait;

class RoleRepository extends Repository
{
	use RepositorieableTrait;

	public function model()
	{
		return "Modules\User\Entities\Role";
	}

	
}