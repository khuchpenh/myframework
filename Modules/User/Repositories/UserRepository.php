<?php 

namespace Modules\User\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Modules\Core\Supports\Traits\RepositorieableTrait;

class UserRepository extends Repository
{
	use RepositorieableTrait;

	public function model()
	{
		return "Modules\User\Entities\User";
	}

	public function listUser()
	{
		$query = $this->model->select('*');
		return $query;
	}
	
}