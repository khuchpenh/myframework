
@extends('layouts.blank')

@section('main_container')
    <!-- page content -->
    <div class="right_col" role="main">
    {!! form_start($form)!!}
    @include('includes.button')
    		<div class="row">
    		
    		</div>
	    		<div class="panel panel-default">
		    		<div class="panel-heading">
		    			<h3 class="panel-title"> <i class="fa fa-th-list" aria-hidden="true"></i> {{ $title }}</h3>
		    		</div>
		    		<div class="panel-body">	
				        <div class="form-group">
				        	<div class="col-md-3">
				        		{!! form_label($form->user_name)!!}
				        	</div>
				        	<div class="col-md-9">
				        		{!! form_widget($form->user_name)!!}
				        	</div>
				        </div>
				        <div class="form-group">
				        	<div class="col-md-3">
				        	{!! form_label($form->email)!!}
				        	</div>
				        	<div class="col-md-9">
				        		{!! form_widget($form->email)!!}
				        	</div>
				        </div>
				        <div class="form-group">
				        	<div class="col-md-3">
				        	{!! form_label($form->role)!!}
				        	</div>
				        	<div class="col-md-9">
				        		{!! form_widget($form->role)!!}
				        	</div>
				        </div>
				        <div class="form-group">
				        	<div class="col-md-3">
				        	{!! form_label($form->first_name)!!}
				        	</div>
				        	<div class="col-md-9">
				        		{!! form_widget($form->first_name)!!}
				        	</div>
				        </div>
				        <div class="form-group">
				        	<div class="col-md-3">
				        	{!! form_label($form->last_name)!!}
				        	</div>
				        	<div class="col-md-9">
				        		{!! form_widget($form->last_name)!!}
				        	</div>
				        </div>
				        <div class="form-group">
				        	<div class="col-md-3">
				        	{!! form_label($form->dob)!!}
				        	</div>
				        	<div class="col-md-9">
				        		{!! form_widget($form->dob)!!}
				        	</div>
				        </div>
				        <div class="form-group">
				        	<div class="col-md-3">
				        	{!! form_label($form->phone)!!}
				        	</div>
				        	<div class="col-md-9">
				        		{!! form_widget($form->phone)!!}
				        	</div>
				        </div>
		    		</div>
		    	</div>
    	@includeIf('includes.button_footer',['model'=>$model])
        {!! form_end($form)!!}
    </div>
    <!-- /page content -->
@endsection

@section('script')
	<script type="text/javascript">
            $(function () {
                $('#dob_at').datetimepicker({
                		format: 'DD/MM/YYYY'
                });
            });
    </script>
@endsection