@extends('layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    {!! form_start($form)!!}
    	<div class="col-md-6 col-sm-12 col-xs-12">
    		<div class="row">
    		
    		</div>
	    		<div class="panel panel-default">
		    		<div class="panel-heading">
		    			<h3 class="panel-title"> <i class="fa fa-th-list" aria-hidden="true"></i> {{ $title }}</h3>
		    		</div>
		    		<div class="panel-body">	
	    				<div class="col-md-3 col-sm-12 col-xs-12">
	    					{!! form_label($form->name)!!}
	    				</div>
	    				<div class="col-md-6 col-sm-12 col-xs-12">
	    					{!! form_widget($form->name)!!}
	    				</div>

	    				<div class="col-sm-12 col-xs-12">
	    				<hr>
	    					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{lang('Save')}}</button>
	    				</div>		
		    		</div>
		    	</div>
    	</div>

    	<div class="col-md-6 col-sm-12 col-xs-12">
    		<div class="panel panel-default">
	    		<div class="panel-heading">
	    			<h3 class="panel-title"> <i class="fa fa-lock" aria-hidden="true"></i> {{ lang('Permission')}}</h3>
	    		</div>
	    		<div class="panel-body">
	    			<div class="form-group">
	    				@if(isset($html))	
			    		 	{!! $html !!}
			    		@endif
	    			</div>
	    		</div>
	    	</div>
    	</div>
        {!! form_end($form)!!}
    </div>
    <!-- /page content -->
@endsection

@section('script')
	
@endsection