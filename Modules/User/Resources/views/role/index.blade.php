@extends('layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<div class="row">

    	</div>
    	<div class="row">
    		<div class="panel panel-primary">
	    		<div class="panel-heading">
	    			<h3 class="panel-title"> <i class="fa fa-th-list" aria-hidden="true"></i> {{ $title }}</h3>
	    		</div>
	    		<div class="panel-body">
	    			{!! $dataTable->table() !!}
	    		</div>
	    	</div>
    	</div>
    	
    </div>
    <!-- /page content -->
@endsection

@section('script')
	{!!$dataTable->scripts()!!}
    <script>
        
    </script>
    
@endsection