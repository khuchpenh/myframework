<?php

Menu::modify('sidebar', function($menu)
{
	$menu->dropdown('User', function ($sub) {
        $sub->url(route('user.index'), 'Manage user');
        $sub->url(route('role.index'), 'Role');
    }, 3, ['icon' => 'fa fa-user']);
});