<?php

return [

    'styles' => [
        'navbar' => Modules\Core\Presenters\Bootstrap\NavbarPresenter::class,
        'navbar-right' => Modules\Core\Presenters\Bootstrap\NavbarRightPresenter::class,
        'nav-pills' => Modules\Core\Presenters\Bootstrap\NavPillsPresenter::class,
        'nav-tab' => Modules\Core\Presenters\Bootstrap\NavTabPresenter::class,
        'sidebar' => Modules\Core\Presenters\Bootstrap\SidebarMenuPresenter::class,
        'navmenu' => Modules\Core\Presenters\Bootstrap\NavMenuPresenter::class,
    ],

    'ordering' => false,

];
