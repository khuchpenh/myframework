<div class="row" style="padding-top:15px !important; padding-bottom: 15px !important">
<div class="col-md-6">
	@if(Auth::user()->can('user.create') || Auth::user()->is_super_admin==true)
		<a href="{{route($model.'.create')}}" class="btn btn-success">
		<i class="fa fa-plus-circle" aria-hidden="true"></i> {{lang('Add') .' '. ucwords(lang($model))}}</a>
	@endif
</div>
</div>