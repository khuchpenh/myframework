<div class="row">
<div class="col-md-6">
	<h3>
	@if(isset($title))
		{{$title}}
	@endif
	</h3>
</div>
<div class="col-md-6 pull-right">
	<a href="{{route($model.'.index')}}" class="btn btn-danger pull-right">
	<i class="fa fa-times-circle-o" aria-hidden="true"></i> 
	{{lang('Cancel')}}</a>
	@if(Auth::user()->can('user.edit') || Auth::user()->is_super_admin==true)
		<button type="submit" class="btn btn-primary pull-right">
		<i class="fa fa-floppy-o" aria-hidden="true"></i> {{lang('Save')}}</button>
	@endif

</div>
</div>