<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{env('COMPANY')}} | </title>

        <link href="{{ asset("css/app.css") }}" rel="stylesheet">

        @stack('stylesheets')

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                 
                @include('includes/sidebar')

                @include('includes/topbar')

                @yield('main_container')

                @include ("includes/footer")

            </div>
        </div>
        <script src="{{ asset("js/vendor.js") }}"></script>
        @include('includes.message')
        
        @stack('scripts')

        @yield('script')

        <script type="text/javascript">
            
            function clickDestroy(_this){
            
            var url  =   $(_this).attr('href');
            swal({   
                title: "{{ lang('Are you sure?') }}",   
                text: "{{ lang('Delete this item, it will be deleted all its relevants.') }}",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "{{ lang('Yes, delete') }}",   
                cancelButtonText: "{{ lang('No, Cancel') }}",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){

                if (isConfirm)
                {
                    $.ajax({
                        url:url,
                        type: "DELETE",
                        data: {id:2, _token:"{{ csrf_token() }}"},
                        success: function(result)
                        {
                            if(result=="success"){
                              
                            }else{
                                swal("Warning!", "This user is used by other or you don't have permission!", "error");
                            }
                            
                        }
                    });
                }
                
            });

            return true;
        }
        </script>

    </body>
</html>