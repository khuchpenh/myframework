@if (Session::has('flash_notification.message'))
<script type="text/javascript">
    toastr.{{ ($level = Session::get('flash_notification.level')) == 'danger' ? 'error' : $level }}("{{ Session::get('flash_notification.message') }}");
</script>
@endif