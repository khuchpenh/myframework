let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.copy([
    "vendor/bower_components/gentelella/vendors/font-awesome/fonts/",
   "vendor/bower_components/gentelella/vendors/bootstrap/fonts/"
    ],"public/fonts/");

mix.copy([
    'vendor/bower_components/gentelella/vendors/iCheck/skins/flat/flat.png',
    'vendor/bower_components/gentelella/vendors/iCheck/skins/square/square.png'
    ],
    'public/css/');

mix.combine([
        'vendor/bower_components/gentelella/vendors/jquery/dist/jquery.min.js',
        'vendor/bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js',
        'vendor/bower_components/datatables.net/js/jquery.dataTables.min.js',
        'vendor/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
        'vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js',
        'vendor/bower_components/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
        'vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js',
        'vendor/yajra/laravel-datatables-buttons/src/resources/assets/buttons.server-side.js',
        'vendor/bower_components/gentelella/vendors/iCheck/icheck.js',
        'resources/assets/js/notify.min.js',
        'vendor/bower_components/moment/moment.js',
        'vendor/bower_components/bootstrap-sweetalert/dist/sweetalert.min.js',
        'vendor/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        'resources/assets/js/custom.js',
    ],'public/js/vendor.js');

mix.combine([
    'vendor/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css',
    'vendor/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
    'vendor/bower_components/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
    'vendor/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css',
    'vendor/bower_components/gentelella/vendors/iCheck/skins/flat/flat.css',
    'vendor/bower_components/gentelella/vendors/iCheck/skins/square/square.css',
    'vendor/bower_components/bootstrap-sweetalert/dist/sweetalert.css',
    'vendor/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'
    'resources/assets/css/custom.css',
    ],'public/css/app.css');

